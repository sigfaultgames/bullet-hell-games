class GameInstance {
  constructor() {
    
  }

  render(){
    //world space is -400x to 400x and -200y to 200y

    // we create a piece of geometry
    var geometry = new THREE.Geometry();
    // we create three points - x y z
    var v1 = new THREE.Vector3(0, 0, 0);
    var v2 = new THREE.Vector3(400, 0, 0);
    var v3 = new THREE.Vector3(400, 200, 0);
    //add these points to a triangle
    var triangle = new THREE.Triangle(v1, v2, v3);
    // create a normal
    var normal = triangle.getNormal(new THREE.Vector3( 0, 0, -1 ));
    //push the vertices to the geometry
    geometry.vertices.push(triangle.a);
    geometry.vertices.push(triangle.b);
    geometry.vertices.push(triangle.c);
    //add a face to the geometry connecting the three vertices - nubers are the index number of the vertex in the geomtry (order they were pushed)
    geometry.faces.push(new THREE.Face3(0, 1, 2, normal));
    // create a colour for our material
    var color = new THREE.Color( "#90EE90" );
    // create a material for our geometry
    var material = new THREE.MeshBasicMaterial();
    //add the colour to our material
    material.color = color;
    // create a mesh out of the geometry and the material
    var mesh = new THREE.Mesh(geometry, material);
    // add the mesh to our scene
    scene.add(mesh);

  }

  processInputRelease(event){
    var keyCode = event.keyCode;
    if(keyCode == 40){
      //example
    }
  }

  processInputPress(event){

    var keyCode = event.keyCode;
    if(keyCode == 32){
      //example
    }
  }

  processWheel(event){
    var scrollamount = event.deltaY;
    
  }

  updateMainLoop(){
    this.currentTime = performance.now();
    this.delta = (this.currentTime - this.lastTime)/1000;
    this.lastTime = this.currentTime;
    this.render()
  }
}