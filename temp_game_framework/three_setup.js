// Converts from degrees to radians.
Math.radians = function(degrees) {
return degrees * Math.PI / 180;
};
// Converts from radians to degrees.
Math.degrees = function(radians) {
return radians * 180 / Math.PI;
};

var currentInstance = new GameInstance();

document.addEventListener('keydown', function(event) {
    currentInstance.processInputPress(event);
});
document.addEventListener('keyup', function(event) {
    currentInstance.processInputRelease(event);
});
document.addEventListener('wheel', function(event) {
    currentInstance.processWheel(event);
});

function sleep(milliseconds) {
var start = new Date().getTime();
for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
    break;
    }
}
} 

function shuffleArray(arrayIn){
    var ArrayInCopy = [];
    for(var i = 0; i < arrayIn.length; i++){
    ArrayInCopy[i] = arrayIn[i];
    }
    var arrayOut = [];

    while(arrayOut.length != arrayIn.length){
    which = Math.floor((Math.random() * ArrayInCopy.length));
    arrayOut.push(ArrayInCopy[which]);
    ArrayInCopy.splice(which, 1);

    }
    return arrayOut;
}

window.addEventListener( 'resize', onWindowResize, false );
function onWindowResize(){
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
}

var scene = new THREE.Scene();
var width = 800;
var height = 400;
var near = 0;
var far = 50;
var camera = new THREE.OrthographicCamera( width / - 2, width / 2, height / 2, height / - 2, near, far );

var renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

currentInstance.drawWorld();
currentInstance.setCameraToRotate();
currentInstance.updateLighting();

function animate() {
    renderer.clear();
    requestAnimationFrame( animate );
    var time = Date.now() * 0.001;
    currentInstance.updateMainLoop();
    renderer.render( scene, camera );
}

function removeEntity(object) {
    var selectedObject = scene.getObjectByName(object.name);
    scene.remove( selectedObject );
}