# Intro
A level file is a json file to define the enemies, spawn patterns, bullets, attack patterns, bullet spells

Root of the json blob is defined as such:

```
{
    "sprite_sheets": [],
    "enemies": [],
    "bullets": [],
    "level_stages": [],
    "spawn_patterns": [],
    "attack_patterns": [],
    "bullet_spells": []
}
```

Each item is a list of objects

## Sprite Sheet Schema

A sprite schema consists of the following fields:

* identifier : A unique identifier to refer to this sprite sheet
* sprite_sheet_url : the url to the image used for the sprite sheet
* max_sprite_animation : number of sprites in sprite sheet animation
* sprite_width : width of an individual sprite (pixels)
* sprite_height : height of an individual sprite (pixels)
* sprite_animation_speed : speed the sprite sheet is iterated through (seconds)

```
{
    "idenitifer": 89,
    "sprite_sheet_url": "/enemy_sprite_one.jpg",
    "max_sprite_animation": 7,
    "sprite_width": 40,
    "sprite_height": 40,
    "sprite_animation_speed": 0.1
}
```

## Enemy Schema

An enemy object consists of the following fields:

* identifier : A unique identifier to refer to this enemy
* sprite_sheet_identifier : the identifier of the sprite sheet the enemy will use
* health : the health of the enemy
* score : the score value of the enemy when killed
* show_health_bar: whether the enemies health is added to the health bar
* attack_pattern_identifier : the identifier of the attack pattern the enemy will use

```
{
    "identifier": 1,
    "sprite_sheet_identifier": 89,
    "health": 999,
    "score": 50,
    "show_health_bar": false,
    "attack_pattern_identifier": 72
}
```


## Bullet Schema

A bullet object consists of the following fields:

* identifier : A unique identifier to refer to this bullet
* passthrough : Does the bullet pass through the player on enemy on hit, or disappear
* destructable : Can the bullet be destroyed by a player bomb
* bullet_collision_radius : the distance the player must be from the bullet to collide (pixels)
* graze_radius : distance player must be within to score graze (pixels)
* sprite_sheet_identifier : the identifier of the sprite sheet the enemy will use

```
{
    "identifier": 1,
    "passthrough": false,
    "destructable": true,
    "bullet_collision_radius": 40
    "graze_radius": 2,
    "sprite_sheet_identifier": 89
}
```

## level Stage Schema

A level stage object consists of the following fields:

* index : a unique integer value respresenting the stage and the order it will occur in the level
* stage-type : enum for stage type
    * FIXED_TIME : Stage ends after a given time (time_out no longer optional field)
    * ENEMY_DEATH : Stage ends ocne all enemy are killed (will auto include a timeout after last enemy is spawned)
* time_out : length of time stage runs for (only applicable when used with FIXED_TIME stage type)

```
{
    "index": 1,
    "stage_type": "FIXED_TIME",
    "time_out": 500
}
```

## Spawn Pattern Schema

A spawn pattern object consists of the following fields:

* stage_index : the level stage this spawn pattern occurs within
* spawn_time : the time the spawning begins after the start of the stage (seconds)
* spawn_type : The type of spawning
    * MULTIPLE : Multiple enemies will be spawned (spawn_count becomes mandatory)
    * ENDLESS : Enemies will keep being spawned until the stage ends
* spawn_count : how many enemies are spawned (only applicable when used with MULTIPLE spawn_type)
* spawn_delay : delay between spawning enemies (seconds)
* spawn_x : X position the spawning occurs
* spawn_y : Y position spawning occurs
* enemy_identifier : the identifier of the enemy that will be spawned

```
{
    "stage_index": 1,
    "spawn_time": 5
    "spawn_type": "MULTIPLE",
    "spawn_count": 500,
    "spawn_delay": 1,
    "spawn_x": 400,
    "spawn_y": 400,
    "enemy_identifier": 1
}
```

## Attack Pattern Schema

An attack pattern object consists of the following fields:

* identifier : A unique identifier to refer to this attack pattern
* attack_pattern_steps : A list of Attack pattern steps to follow (see below for definition)

```
{
    "identifier": 1,
    "attack_pattern_steps": []
}
```

### Attack Pattern Step Schema

An attack pattern step object consists of the following fields:

* index : the index position the step occurs in
* bullet_spell_identifiers : the identifiers of the bullet spells to use
* step_type : the type of step to use
    * FIXED_TIME : step only lasts for the given time (time_out becomes mandatory)
    * TILL_DEATH : step lasts until health of user is <= 0
    * TILL_DEATH_RESET : step lasts until health of user is <= 0 and resets health of user when step is complete

```
{
    "index": 1,
    "bullet_spell_identifiers": [],
    "step_type": FIXED_TIME,
    "time_out": 20
}
```

## Bullet Spell Schema

An bullet spell object consists of the following fields:

* identifier : A unique identifier to refer to this bullet spell
* bullet_spell_steps : A list of bullet spell steps to follow (see below for definition)
* bullet_identifier : the identifier of the bullet the bullet spell will use
* start_delay : time delay before first wave is spawned
* spawn_count : number of bullets spawned
* degrees_seperation : angle between each spawned bullet (degrees)
* spawn_start_angle : angle bullets that spawning (0 degrees is to the right "positive X")(degrees)
* wave_count : number of waves of bullet spawn
* wave_delay : delay between each wave (seconds)
* spawn_start_angle_wave_change : changes the spawn_start_angle by the given value for every wave

```
{
    "identifier": 1,
    "bullet_spell_steps": [],
    "bullet_identifier": 2,
    "start_delay": 5
    "spawn_count": 3,
    "degrees_seperation": 50,
    "spawn_start_angle": 270,
    "wave_count" : 3,
    "wave_delay": 30,
    "spawn_start_angle_wave_change" : -30
}
```

### Bullet Spell Step Schema

An bullet spell step object consists of the following fields:

* index : the index position the step occurs in
* data : OneOf Step Object Type (See Below)

```
{
    "index": 1,
    "data": {}
}
```

#### Bullet Spell Step Data Types

CONTINUE_CURRENT_VECTOR

* speed : speed bullet travels vector (pixels)

```
{
    "type" : CONTINUE_CURRENT_VECTOR,
    "speed" : 2
}
```

CONTINUE_CURRENT_VECTOR_FOR_DISTANCE

* speed : speed bullet travels vector
* distance : distance bullet should travel

```
{
    "type" : CONTINUE_CURRENT_VECTOR,
    "speed" : 2,
    "distance": 50
}
```

CONTINUE_CURRENT_VECTOR_UNTIL_START_RADIUS

* speed : speed bullet travels vector
* start_radius : radial distance from start position bullet travels until

```
{
    "type" : CONTINUE_CURRENT_VECTOR_UNTIL_START_RADIUS,
    "speed" : 2,
    "start_radius": 50
}
```

CONTINUE_CURRENT_VECTOR_UNTIL_PLAYER_RADIUS

* speed : speed bullet travels vector
* start_radius : radial distance from player position bullet travels until

```
{
    "type" : CONTINUE_CURRENT_VECTOR_UNTIL_PLAYER_RADIUS,
    "speed" : 2,
    "player_radius": 50
}
```

REVECTOR_STRAIGHT_TO_PLAYER

```
{
    "type" : REVECTOR_STRAIGHT_TO_PLAYER
}
```

CURVE_TO_PLAYER

* curve_apex_size : apex size of the curve
* prefered_curve_direction : enum (POSITIVE , NEGATIVE) whether to curve in positive along x/y or negative

```
{
    "type" : CURVE_TO_PLAYER,
    "curve_apex_size" 20,
    "preferred_curve_direction": POSITIVE
}
```

SPAWN_NEW_BULLET_SPELL

* bullet_spell_identifier : identifier of new bullet spell to spawn
* destroy_original : whether to destroy the original bullet

```
{
    "type" : SPAWN_NEW_BULLET_SPELL,
    "bullet_spell_identifier" 22,
    "destroy_original": true
}
```

REPEAT_SPAWN_NEW_BULLET_SPELL

* bullet_spell_identifier : identifier of new bullet spell to spawn
* destroy_original : whether to continue movement on the original bullet
* spawn_count : how many copies of the bullet spell to spawn
* spawn_delay : time delay between bullet spell spawning

```
{
    "type" : REPEAT_SPAWN_NEW_BULLET_SPELL,
    "bullet_spell_identifier" 22,
    "continue_movement": true,
    "spawn_count": 5,
    "spawn_delay": 30
}
```

FOLLOW_QUADRATIC

* x_equation : equation used to calculate X position
* y_equation : equation used to calculate Y position

Avaliable variables for equations:

* X : current X
* Y : current Y
* T : current time since start of step
* SX : starting X (from start of step)
* SY : starting Y (from start of step)
* PX : player X
* PY : player Y

```
{
    "type" : FOLLOW_QUADRATIC,
    "x_equation" "(X*3T),
    "y_equation" "(Y+5T)
}
```

FOLLOW_QUADRATIC_FOR_TIME

* x_equation : equation used to calculate X position
* y_equation : equation used to calculate Y position
* time : time to follow the equation for (seconds)

Avaliable variables for equations:

* X : current X
* Y : current Y
* T : current time since start of step
* SX : starting X (from start of step)
* SY : starting Y (from start of step)
* PX : current player X
* PY : current player Y

```
{
    "type" : FOLLOW_QUADRATIC_FOR_TIME,
    "x_equation": "X*3T",
    "y_equation": "Y+5T",
    "time": 20
}
```