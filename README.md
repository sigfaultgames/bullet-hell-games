# HOW TO USE

## Install

### Windows

Download GNUWin32 accepting all defaults
```
https://sourceforge.net/projects/gnuwin32/files/make/3.81/make-3.81.exe/download?use_mirror=kumisystems&download=
```
add the following to your system path:
```
C:\Program Files (x86)\GnuWin32\bin\
```
in powershell run:
```
make install
```

### Linux/Mac
in terminal run
```
make install
```

## Run

to run do:
```
make start
```

## Generate Migrations

to run do:
```
make start_database
```

once database is ready:
```
make run_migration
make generate_migration {COMMITMESSAGE}
```

# ABOUT

## Front End

includes the base HTML page and the javscript project


## Back End

includes the DB models and the api

To run api locally:
```
pip install -r requirements.txt
python run.py
```

# DESIGN

## API Endpoints:
* Login  : returns a token
* Get User : returns user data
* Get Level : returns level data for a given level ID
* Get Random Level : returns data for a random level
* Record Stats : validates (maybe if we can figure it out) and saves your stats against a level (time/score/damage taken/enemies killed)
* Upload Level : validates and saves level json to DB 
* Register User : registers a new user
* Delete Level : deletes a levels

## Architecture

```
|----------|     |-----------|     |----------|
|Front End |---->| Flask API |---->| MYSQL DB |
|----------|     |-----------|     |----------|
```

## Database
* Users : id, username, hashed_password, salt, user_type, created_at
* Tokens : user_id, token, created_at, expires_at
* User Events : user_id, event_type, created_at
* Levels : id, level_data, user_id, created_at, deleted_at
* User Level Stats : user_id, level_id, score, time_taken, damage_taken, enemies_killed, created_at
* User Event Type : id, name
* User type : id, user_type