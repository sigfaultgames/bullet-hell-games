import pytest
import json

@pytest.mark.usefixtures("add_db_data_for_testing")
class TestAPI:
    #----------------------------------------------------------------------------------------------------
    # TEST LOGIN
    #----------------------------------------------------------------------------------------------------

    def test_login_successfull(self, client):
        username = pytest.EXISTING_USERNAME
        password = pytest.EXISTING_USER_PASSWORD

        response = client.get(
            f'/login?username={username}&password={password}',
        )

        data = response.get_json()

        assert response.status_code == 200
        assert "token" in response.data.keys()

    def test_login_bad_input(self, client):
        response = client.get(
            '/login?username=Donkey',
        )

        assert response.status_code == 500

    def test_login_unsuccessful_login(self, client):
        response = client.get(
            '/login?username=NotDonkey&password=Shrek123$',
        )

        assert response.status_code == 401

    #----------------------------------------------------------------------------------------------------
    # TEST USER
    #----------------------------------------------------------------------------------------------------

    def test_user_successfull(self, client):
        token = username = pytest.EXISTING_USER_TOKEN

        headers = {
            "token": token
        }

        response = client.get(
            '/user',
            headers=headers,
        )

        data = response.get_json()

        assert response.status_code == 200
        assert "id" in response.data.keys()
        assert "username" in response.data.keys()
        assert "user_type" in response.data.keys()
        assert "created_at" in response.data.keys()

    #----------------------------------------------------------------------------------------------------
    # TEST REGISTER USER
    #----------------------------------------------------------------------------------------------------

    def test_register_user_successfull(self, client):

        data = {
            "username": "GDawg",
            "password": "qwertyuiop"
        }

        response = client.post(
            '/register_user',
            json=data
        )

        data = response.get_json()

        assert response.status_code == 201
        assert "token" in response.data.keys()