from flask import request
from typing import Dict
from flask import g
from datetime import datetime, timedelta

from models import models
from models.user_level_stat import UserLevelStat
from models.level import Level
from models.user import User
from models.token import Token

import settings
import os
import sys

sys.path.append(os.path.dirname(os.path.realpath(__file__ + os.sep + os.pardir)))
from common.DAO import DAO
from uuid import uuid4
import hashlib
#These functions need to be modified so that the API endpoints can communicate with the database. 
#Needs an appropriate import. Information that needs to be queried comes from the database. Info that needs to be put into the database
#needs a class to be sent into it. ///Imports are done.
#Refer to swagger edditor for what needs to be put into classes. Look at the request body. The keys are
#columns. The function in question must ask for the appropriate Key from body to work.

def login(username: str, password: str):
    with DAO.session_scope(settings.DATABASE_URL) as session:
        print("DO STUFF HERE")

    return 'hello world', 200

def get_user(token: str):
    with DAO.session_scope(settings.DATABASE_URL) as session:
        user_token = session.query(Token).get(token)
        user = session.query(User).get(user_token.user_id)

    return user.to_dict(), 200

def register_user():
    #Add code to verify our request is okay
    body = request.get_json()
    username=body['username']
    password=body['password']
    #Add code to ensure the insertion of the user is okay
    with DAO.session_scope(settings.DATABASE_URL) as session:
        id=str(uuid4())
        salt=str(uuid4())
        hashed_password=hashlib.sha512(bytes(salt+password, 'utf-8')).hexdigest()
        user_type_id=2
        created_at=datetime.now()
        user_register = User(
            id=id, 
            username=username,
            hashed_password=hashed_password, 
            salt=salt,
            user_type_id=user_type_id, 
            created_at=created_at)
        session.add(user_register)
        session.commit()
        token = Token(user_id=user_register.id, expires_at=datetime.now() + timedelta(hours=1))
        session.add(token)
        session.commit()
        session.refresh(token)
        token_string = str(token.token)
    #Add code for loging in the user and returning a successful 201 response
    return {"token" : token_string}, 201

def get_random_level(token: str):
    with DAO.session_scope(settings.DATABASE_URL) as session:
        randlevel = session.query(Level).order_by(sqlalchemy.func.random()).one()
        randlevel_dict = randlevel.to_dict()
        
    return randlevel_dict, 200

def upload_level(token: str):
    body = request.get_data()
    with DAO.session_scope(settings.DATABASE_URL) as session:
        token_object = session.query(Token).get(token)
        ulevel = Level(data=body['data'], user_id=token_object.user_id)
        session.add(ulevel)
        session.commit()
        session.refresh(ulevel)
        
    return ulevel.to_dict(), 200

def get_level(token: str, id: str):
    with DAO.session_scope(settings.DATABASE_URL) as session:
        level = session.query(Level).get(id)
        level_dict = level.to_dict()
        
    return level_dict, 200

def delete_level(token: str, id: str):
    with DAO.session_scope(settings.DATABASE_URL) as session:
        level_delete = session.query(Level).get(id)
        session.delete(level_delete)
        session.commit()
        
    return 'success', 200

def record_stats(token: str):
    body = request.get_data()
    with DAO.session_scope(settings.DATABASE_URL) as session:
        stats = UserLevelStat(user_id=body['user_id'], level_id=body['level_id'], score=body['score'], time_taken=body['time_taken'], 
        damage_taken=body['damage_taken'], enemies_killed=body['enemies_killed'])
        session.add(stats)
        session.commit()
        
    return 'success', 201
