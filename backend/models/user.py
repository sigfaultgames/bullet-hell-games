import uuid
from datetime import datetime
from uuid import uuid4
from sqlalchemy import Column, ForeignKey, String, DateTime, Integer
from sqlalchemy.orm import relationship
from common.helper import generate_string_uuid4
import os
import sys

sys.path.append(os.path.dirname(os.path.realpath(__file__ + os.sep + os.pardir)))
from common.DAO import DAO

Base = DAO.get_base_mapper()

class User(Base):
    """
    The user class represents the users of the system.
    """
    __tablename__ = 'user'
    
    id: str = Column(
        String(36),
        primary_key=True,
        default=generate_string_uuid4
    )
    
    username: str = Column(
        String(128),
        nullable=False,
        index=True,
        unique=True
    )

    hashed_password: str = Column(
        String(256),
        nullable=False
    )

    salt: str = Column(
        String(128),
        nullable=False
    )

    user_type_id: int = Column(
        Integer(),
        ForeignKey('user_type.id'),
        nullable=False
    )

    created_at: datetime = Column(
        DateTime(),
        nullable=False,
        default=datetime.now
    )

    user_events: relationship = relationship("UserEvent", lazy="dynamic", cascade="merge")

    user_level_stats: relationship = relationship("UserLevelStat", lazy="dynamic", cascade="merge")

    levels: relationship = relationship("Level", lazy="dynamic", cascade="merge")
    
    token: relationship = relationship("Token", lazy="dynamic", cascade="merge")

    def to_dict (self):
        return {
            "id": self.id,
            "username": self.username,
            "user_type": self.user_type_id,
            "created_at": self.created_at,
        }

