import uuid
from datetime import datetime
from uuid import uuid4
from sqlalchemy import Column, ForeignKey, DateTime, String
from sqlalchemy.orm import relationship
import os
import sys

sys.path.append(os.path.dirname(os.path.realpath(__file__ + os.sep + os.pardir)))
from common.DAO import DAO

Base = DAO.get_base_mapper()

class Token(Base):

    """
    Stores user's authentication on a token.
    """
    
    __tablename__ = 'token'

    def create_uuid(context):
        return str(uuid4())

    token: str = Column(
        String(36),
        primary_key=True,
        default=create_uuid
    )

    user_id: str = Column(
        String(36),
        ForeignKey("user.id"),
        nullable=False
    )

    created_at: datetime = Column(
        DateTime(),
        nullable=False,
        default=datetime.now
    )

    expires_at: datetime = Column(
        DateTime(),
        nullable=False
    )



    user: relationship = relationship("User")
    
