import uuid
from datetime import datetime
from uuid import uuid4
from sqlalchemy import Column, ForeignKey, DateTime, Float, Integer, String
import os
import sys

sys.path.append(os.path.dirname(os.path.realpath(__file__ + os.sep + os.pardir)))
from common.DAO import DAO

Base = DAO.get_base_mapper()

class UserLevelStat(Base):
    """
    Tracks a user's in-game stats.
    """
    __tablename__ = 'user_level_stats'

    id: int = Column(
        Integer(),
        nullable=False,
        primary_key=True
    )

    user_id: str = Column(
        String(36),
        ForeignKey("user.id", ondelete="cascade"),
        nullable=False
    )

    level_id: str = Column(
        String(36),
        ForeignKey("level.id", ondelete="cascade"),
        nullable=False
    )

    score: float = Column(
        Float(),
        nullable=False
    )

    time_taken: float = Column(
        Float(),
        nullable=False
    )

    damage_taken: int = Column(
        Integer(),
        nullable=False
    )

    enemies_killed: int = Column(
        Integer(),
        nullable=False
    )

    created_at: datetime = Column(
        DateTime(),
        nullable=False,
        default=datetime.now
    )





