from sqlalchemy import Column, String, Integer
import os
import sys

sys.path.append(os.path.dirname(os.path.realpath(__file__ + os.sep + os.pardir)))
from common.DAO import DAO

Base = DAO.get_base_mapper()

class UserEventType(Base):

    """
    Tracks events triggered by user.
    """

    __tablename__= 'user_event_type'

    id: int = Column(
        Integer(),
        nullable=False,
        primary_key=True
    )

    name: str = Column(
        String(32),
        nullable=False
    )
