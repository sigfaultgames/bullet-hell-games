import uuid
from datetime import datetime
from uuid import uuid4
from sqlalchemy import Column, ForeignKey, DateTime, Integer, String
import os
import sys

sys.path.append(os.path.dirname(os.path.realpath(__file__ + os.sep + os.pardir)))
from common.DAO import DAO

Base = DAO.get_base_mapper()

class UserEvent(Base):
    """
    Tracks user generated events.
    """
    __tablename__ = 'user_event'

    id: int = Column(
        Integer(),
        primary_key=True
    )

    user_id: str = Column(
        String(36),
        ForeignKey("user.id"),
        nullable=False
    )

    user_event_type_id: int = Column(
        Integer(),
        ForeignKey("user_event_type.id")
    )

    created_at: datetime = Column(
        DateTime(),
        nullable=False,
        default=datetime.now
    )


    

