import uuid
from datetime import datetime
from uuid import uuid4
from sqlalchemy import Column, ForeignKey, DateTime, String
from sqlalchemy.dialects.mysql import JSON
from sqlalchemy.orm import relationship
from typing import Dict
import os
import sys

sys.path.append(os.path.dirname(os.path.realpath(__file__ + os.sep + os.pardir)))
from common.DAO import DAO

Base = DAO.get_base_mapper()

class Level(Base):

    """
    Tracks data relating to a level.
    """

    __tablename__ = 'level'

    id: str = Column(
        String(36),
        primary_key=True,
        default=uuid4
    )

    level_data: Dict = Column(
        String(30000),
        nullable=False
    )

    user_id: str = Column(
        String(36),
        ForeignKey("user.id"),
        nullable=False
    )

    created_at: datetime = Column(
        DateTime(),
        nullable=False,
        default=datetime.now
    )

    expires_at: datetime = Column(
        DateTime(),
        nullable=False
    )

    user: relationship = relationship("User", cascade="merge")


    def to_dict (self):
        return {
            "id": self.id,
            "data": self.level_data,
            "created_by": self.user.username,
            "created_at": self.created_at,
        }



