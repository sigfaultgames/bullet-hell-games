from sqlalchemy import Column, String, Integer
import os
import sys

sys.path.append(os.path.dirname(os.path.realpath(__file__ + os.sep + os.pardir)))
from common.DAO import DAO

Base = DAO.get_base_mapper()

class UserType(Base):
    """
    Defines security level of a user.
    """

    __tablename__ = 'user_type'

    id: int = Column(
        Integer(),
        nullable=False,
        primary_key=True
    )

    user_type: str = Column(
        String(32),
        nullable=False
    )
