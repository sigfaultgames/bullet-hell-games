from contextlib import contextmanager
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base


class DAO:

    database_url = None
    db_engine = None
    session_maker = None
    base = None

    @classmethod
    def engine(cls, database_url=None):

        if cls.db_engine is not None and database_url is not None:
            cls.db_engine.dispose()

        if database_url is not None:
            cls.database_url = database_url
            cls.db_engine = create_engine(cls.database_url, connect_args={'connect_timeout': 10})

        return cls.db_engine

    @classmethod
    def get_session(cls, database_url=None, autocommit=False, autoflush=False):
        if database_url is not None:
            cls.engine(database_url)

        Session = sessionmaker(bind=cls.db_engine, autocommit=autocommit, autoflush=autoflush)

        return Session()

    @classmethod
    def get_base_mapper(cls):
        if cls.base is None:
            cls.base = declarative_base()
        return cls.base

    @classmethod
    @contextmanager
    def session_scope(cls, database_url=None, reconnect=False):
        """
        Provide a transactional scope around a series of operations.
        If neither a database_url or reconnect flag have been given we simply yield to the caller
        :paramters database_url, reconnect:
        :yield db_session:
        :return:
        """
        if (database_url is None or len(database_url) == 0) and not reconnect:
            yield None
            return

        if database_url is not None:
            cls.database_url = database_url

        assert database_url is not None, "A database_url is required."

        # There are two scenarios to get here:
        # 1) a database_url has been supplied
        # 2) a database_url was supplied with the class instantiation and the reconnect flag has been supplied
        session = cls.get_session(database_url=cls.database_url, autocommit=False, autoflush=False)
        try:
            yield session
            session.commit()
        except Exception:
            session.rollback()
            raise
        finally:
            session.close()


