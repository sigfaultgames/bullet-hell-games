from uuid import uuid4

def generate_string_uuid4(context):
    '''
    helper function for generate string uuid for sqlalchemy defaults
    '''
    return str(uuid4())
