# Bullet Hell Game Frontend

## Installation
`
sudo apt install npm
npm install
`

## Building the application
`
npm run build
`

## Starting the application
`
npm run start
`

## Accessing the application
`
localhost:3000
`


