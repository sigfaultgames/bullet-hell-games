//webpack.config.js
var HtmlWebpackPlugin = require('html-webpack-plugin');
var path = require('path');

module.exports = {
    entry: {
        app: './src/app/main.js',
    },
    output: {
        path: path.join(__dirname, "build"),
        filename: 'js/bundle.js'
    },
    module: {
        loaders: [
            {
                loader:'babel-loader',
                test:/\.js$/,
                exclude:/node_modules/
            }
        ]
    },
    devServer: {
        contentBase: path.join(__dirname, 'build'),
        port:3000
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'index.html'
        })
    ]
};
