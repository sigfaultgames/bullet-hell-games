export DATABASE_URL := mysql+pymysql://root:password@localhost:3306/bullet_hell

install:
	pip install -r backend/requirements.txt
	pip install -r backend/dev-requirements.txt

test:
	docker-compose -f docker-compose-db.yaml up -d
	alembic -c migrations/alembic.ini upgrade head
	- pytest
	- alembic -c migrations/alembic.ini downgrade base
	docker-compose -f docker-compose-db.yaml stop

start:
	docker-compose up

build:
	docker-compose build

start_database:
	docker-compose -f docker-compose-db.yaml up

run_migration:
	alembic -c migrations/alembic.ini upgrade head

generate_migration:
	echo "commit message:"$(COMMITMESSAGE)"
	alembic -c migrations/alembic.ini revision --autogenerate -m "$(COMMITMESSAGE)"
