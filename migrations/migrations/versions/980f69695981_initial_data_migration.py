"""Initial Data Migration

Revision ID: 980f69695981
Revises: 28a02a5fcf37
Create Date: 2020-09-30 19:36:09.225451

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '980f69695981'
down_revision = '28a02a5fcf37'
branch_labels = None
depends_on = None


def upgrade():
    op.execute('INSERT INTO user_type (id, user_type) VALUES (1, "admin")')
    op.execute('INSERT INTO user_type (id, user_type) VALUES (2, "standard")')
    op.execute('INSERT INTO user_event_type (id, name) VALUES (1, "register")')
    op.execute('INSERT INTO user_event_type (id, name) VALUES (2, "login")')

#Add user event types when ready

def downgrade():
    op.execute('SET FOREIGN_KEY_CHECKS = 0')
    op.execute('TRUNCATE TABLE token')
    op.execute('TRUNCATE TABLE user_level_stats')
    op.execute('TRUNCATE TABLE level')
    op.execute('TRUNCATE TABLE user')
    op.execute('TRUNCATE TABLE user_event')
    op.execute('SET FOREIGN_KEY_CHECKS = 1')

    op.execute('DELETE FROM user_type WHERE id IN (1, 2)')
    op.execute('DELETE FROM user_event_type WHERE id IN (1, 2)')