import connexion
import sys
import os
import pytest
import hashlib
from backend.common.DAO import DAO

from datetime import datetime, timedelta
sys.path.insert(0,'./backend')
from models import models
from models.user import User
from models.token import Token

flask_app = connexion.FlaskApp(__name__)
flask_app.add_api('backend/swagger/api.yaml')

def pytest_configure():
    pytest.EXISTING_USERNAME = "DONKEY"
    pytest.EXISTING_USER_PASSWORD = "password"
    pytest.EXISTING_USER_TOKEN = "f56dfef8-c559-4dbf-b131-9ed1bcf12f88"

@pytest.fixture
def client():
    with flask_app.app.test_client() as c:
        yield c

@pytest.fixture
def add_db_data_for_testing():
    with DAO.session_scope(os.getenv("DATABASE_URL")) as session:

        # delete all data  as this is run before any tests
        session.execute('SET FOREIGN_KEY_CHECKS = 0')
        session.execute('TRUNCATE TABLE token')
        session.execute('TRUNCATE TABLE user_level_stats')
        session.execute('TRUNCATE TABLE level')
        session.execute('TRUNCATE TABLE user')
        session.execute('TRUNCATE TABLE user_event')
        session.execute('SET FOREIGN_KEY_CHECKS = 1')

        password = pytest.EXISTING_USER_PASSWORD
        salt = "salt"
        hashed_password = hashlib.sha512(bytes(salt+password, 'utf-8')).hexdigest()

        user = User(
            username=pytest.EXISTING_USERNAME,
            hashed_password=hashed_password,
            salt=salt,
            user_type_id=1
        )
        session.add(user)
        session.commit()
        token = Token(
            token=pytest.EXISTING_USER_TOKEN,
            user_id=user.id,
            expires_at=datetime.now() + timedelta(hours=1))
        session.add(token)
        session.commit()